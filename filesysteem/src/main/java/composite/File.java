package composite;

public class File implements Component {
    private final String name;
    private final long size;
    private Directory parent;

    public File(String name, long size) {
        this.name = name;
        this.size = size;
        this.parent = null;
    }

    @Override
    public long getSize() {
        return this.size;
    }

    @Override
    public String getPath() {
        if (parent != null) {
            return parent.getPath() + "\\" + this.name;
        } else {
            return this.name;
        }
    }

    @Override
    public void setParent(Directory parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return this.getPath() + " (" + this.getSize() + "kb)";
    }
}
