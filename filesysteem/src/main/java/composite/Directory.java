package composite;

import java.util.*;

public final class Directory extends File implements Container {
    private final List<Component> children;
    private Directory parent;

    public Directory(String name) {
        super(name,0);
        children = new ArrayList<>();
    }

    @Override
    public long getSize() {
        long size = 0;
        for (Component c :
                children) {
            size += c.getSize();
        }

        return size;
    }


    public void add(Component c) {
        this.children.add(c);
        c.setParent(this);
    }

    public void remove(Component c) {
        this.children.remove(c);
        c.setParent(null);
    }

}
